---
layout: markdown_page
title: "Diversity, Inclusion & Belonging Sharing Page"
description: "This page provides a place for team members to share any learnings for Ally Learning Groups, DIB Rountables and other DIB initiatives."
canonical_path: "/company/culture/inclusion/DIB-sharing-page/"
---

## Diversity, Inclusion & Belonging Sharing Page 

### Privilege For Sale Sharing 

**Example:**
- Group Members: 
- Privileges Chosen: 
- Reason for Privileges Chosen:
- Learnings from the experience: 

- Group Members: Hannah Schuler, Julia Hill- Wright, Lea Tuizat, Ryan Kimball and Steve Cull
- Privileges Chosen: 2,8,11,20,5,14,18,16,3,7
- Reason for Privileges Chosen:
- Learnings from the experience:

**June 2021 Ally Lab Learning Group 2**

- Group Members: Ethan Urie, Jessica Reeder, Steve Abrams, Naomi Khan, Emily Plotkin, Meghan Maneval, and Isaac Dawson
- Privileges Chosen: 2, 3, 4, 5, 6, 8, 11, 13, 18, 20
- Reason for Privileges Chosen: We began this exercise by individually reviewing the list and “voting” for the privileges we would purchase. We then reviewed the items with the most votes to create our team list of 10 privileges. In many cases, team members voted for items that they may currently take for granted and would be deeply impacted by should that privilege be taken away. In particular, the team felt strongest about privileges related to our bodies, our health and our safety.
- Learnings from the experience: Most of our team members were surprised by the privileges listed and noted that they hadn’t thought about these items too much before. It really solidified the importance of Allyship training as many might take these items for granted. It was deeply impactful to take the mindset of “right now, I have NONE of these privileges, what do I need to take back.” 

### DIB Roundtable Sharing

**Example:**
- Group Members:
- What is one thing as a group can we do to make a positive impact on Diversity, Inclusion and Belonging?:
- What did the group learn from the experience?: 

- Group Members: Hannah Schuler, Julia Hill- Wright, Lea Tuizat, Ryan Kimball, Steve Cull, Shawn Winters, Kaleb Hill
- What is one thing as a group can we do to make a positive impact on Diversity, Inclusion and Belonging?:
1. System of communication to be more honest and candid
1. Hold space for one another and be really, crowd source help and not feel ashamed. 
1. Being empathetic to everyone’s situations, where they came from before being here. Challenging bias.
1. Safe space to talk through tough situations. Not making assumptions. approaching with kindness empathy and respect.
1. Self accountability, do your part to make people feel a part of the team and welcome. 
1. Empathy first. 
1. Asking better questions of other people. Help me understand where other people came from. 

**June 2021 Ally Lab Learning Group 2**

- Group Members: Ethan Urie, Jessica Reeder, Steve Abrams, Naomi Khan, Emily Plotkin, Meghan Maneval, and Isaac Dawson

- What is one thing as a group can we do to make a positive impact on Diversity, Inclusions and Belonging?:
   - Honesty and authenticity
   - Be more focused on learning about others than sharing about ourselves. Being a person who listens and asks questions to genuinely understand and learn about others. I really enjoyed the one talk about building trust - so much of that gets pushed aside in business, but is really important for fostering these qualities. 
   - Being open-minded and accepting and being prepared to be vulnerable an trust in each other.
   - Being conscious of your own empathy and being ok with stepping into an uncomfortable place sometimes and making mistakes.
   - Having genuine conversations about these topics with more and more people as your comfort zone expands. 

### Ally Lab Learning Groups Activity Sharing 

**Example:**
- Group Members:
- Activity:
- Learnings, Actions, or Answers etc:  

**June 2021 Ally Lab Learning Group 2**

- Group Members: Ethan Urie, Jessica Reeder, Steve Abrams, Naomi Khan, Emily Plotkin, Meghan Maneval, and Isaac Dawson
- Learnings, Actions, or Answers etc:  
   - We agreed this was an excellent exercise in uncovering differing perspectives of the same situation.
   - We found our ideas around how and when to act differed and that led to some great discussion deconstructing the various benefits and drawbacks of each approach.
   - A key takeaway was by the end of the activity, we noticed that many of the scenarios exhibited a pattern, pointing to systemic problems and realized that while we were acting to help in each individual situation, we did not necessarily address the problems at a larger scale. This led to a great discussion on what that might look like.

### Ally Lab Learning Groups Committment and Values pledge Sharing

**Example:**
- Group Members
- Committment: As a group we commit to 
- Values: The values we have chosen to ensure we align to out committment are as follows: Example: Action: We will say something when we see a DIB issue

**June 2021 Ally Lab Learning Group 2**

- Group Members: Ethan Urie, Jessica Reeder, Steve Abrams, Naomi Khan, Emily Plotkin, Meghan Maneval, and Isaac Dawson

- We spent time discussing our own outcomes and experience with the program resulting in a few follow-up actions we would like to pursue to maintain our engagement as Allies:
   - Maintain curiosity and to grow understanding and reduce discomfort and ignorance
   - Add Ally Discussion Groups as an onboarding task
   - Draft an OKR to address issues at larger levels (group/department/company)
